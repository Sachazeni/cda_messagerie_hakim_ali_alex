<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Nouveau message</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">

</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="messageRecu">Boite de r�ception </a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="nouveauMessage">Nouveau message <span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link" href="messageEnvoye">Message
						envoy�</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Message
						archiv�</a></li>
			
						
			</ul>
			<form class="form-inline my-2 my-lg-0">
				<button class="btn btn-outline-success my-2 my-sm-0" onclick=""
					type="submit">Deconnexion</button>
			</form>
		</div>
	</nav>

	<form id="form" action="nouveauMesg" method ="post">

		<div class="form-group">
			<label for="destinataireLog" class="col-12 col-md-4">
			Login Destinataires:
			</label> 
			<input type="text" id=destinataireLog name="destinataireLog" class="form-control" placeholder="Login">
			
		</div>

		<div class="form-group">
			<label for="sujet" class="col-12 col-md-4">Sujet :
			</label> 
			<input type="text" id=destinataireLog name="sujet"
				class="form-control" placeholder="Objet de votre message">
		</div>
		<div class="form-group">
			<label for="exampleFormControlTextarea1" class="col-12 col-md-4">Votre
				message: </label>
			<textarea class="form-control" name ="contenuMessage"id="exampleFormControlTextarea1"
				rows="20"></textarea>
		</div>
		<div style="text-align: right">
			<input name="btnValider" type=submit value="Envoyer"
				class="btn btn-outline-success btn-block" />
		</div>
	</form>
	<script src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>

</body>
</html>

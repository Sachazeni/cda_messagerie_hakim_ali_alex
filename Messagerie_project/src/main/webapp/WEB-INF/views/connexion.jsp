<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">

</head>
<body>

	<div class="container-fluid bg">
		<div class="row justify-content-center">
			<div class="container-fluid col-sm-5">
				<h2>Bienvenue sur votre messagerie</h2>
				<form action="accueil" method="post" class="form-container">

					<div class="form-group">
						<label for="login">Login : </label> <input type="text"
							class="form-control" name="login" id="login"
							placeholder="Entrer votre login" required>
					</div>
					<div class="form-group">
						<label for="mot de passe">Mot de passe :</label> <input
							type="password" class="form-control" name="password"
							id="password" placeholder="Entrer votre mot de passe" required>
					</div>

					<button type="submit" name="connexion" class="btn btn-primary">Connexion</button>
					<br> <br>

					<div class="form-group">
					<a href="creationCompte">Cr�ation de compte</a>
					</div>
				</form>

			</div>
		</div>
	</div>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>
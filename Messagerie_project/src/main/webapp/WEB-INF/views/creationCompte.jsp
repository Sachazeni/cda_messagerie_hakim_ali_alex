<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css">
  
</head>
<body>
<div class="container-fluid bg">
<div class="row justify-content-center">
<div class="container-fluid col-sm-5">
	<h2>Cr�ation de compte</h2>
	<form action="add" method="post" class="form-container">
	 
	 <div class="form-group">
						<label for="login">Login : </label> <input type="text"
							class="form-control" name="login" id="login"
							placeholder="Choisir votre login" required>
					</div>
					<div class="form-group">
						<label for="mot de passe">Mot de passe :</label> <input
							type="password" class="form-control" name="password"
							id="password" placeholder="Choisir votre mot de passe" required>
					</div>
					
	  <div class="form-group">
	    <label for="nom">Nom : </label>
	    <input type="text" class="form-control" name="nom" id="nom" placeholder="Entrer votre nom" required>
	  </div>
	  <div class="form-group">
	    <label for="prenom">Prenom :</label>
	    <input type="text" class="form-control" name="prenom" id="prenom" placeholder="Entrer votre prenom" required>
	  </div>
	  <div class="form-group" id="picker">
	  	 <label for="dateNaissance">Date de naissance :</label>
	    <input class="datepicker" data-date-format="mm/dd/yyyy" name="dateNaissance" id="dateNaissance" placeholder="" >
	  </div>  
	  <button type="submit" name="create" class="btn btn-primary">Valider</button>
	</form>
	 
	</div>
	</div>
	</div>
	<script src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
	<script>
	$('.datepicker').datepicker({
		yearRange: '1940:2002',
		format: 'dd/mm/yyyy'

	});
	</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">

</head>
<body>

	<div class="container-fluid bg">
		<div class="row justify-content-center">
			<div class="container-fluid col-sm-5">
				<h2>Bienvenue sur votre espace</h2>
				<form action="accueil" method="post" class="form-container">

					<div class="form-group">
						<a href="<c:url value="accueilMessagerie"/>">Accueil
							Messagerie</a>
					</div>

					<div class="form-group">
						<a href="<c:url value="gestionComptesExistants"/>">Gestion des
							comptes existants</a>
					</div>

					<div class="form-group">
						<a href="<c:url value="gestionNouveauxComptes"/>">Gestion des
							nouveaux comptes</a>
					</div>
					 <button type="submit" onclick="" name="deconnexion" class="btn btn-primary">Deconnexion</button>
					
				</form>

			</div>
		</div>
	</div>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>
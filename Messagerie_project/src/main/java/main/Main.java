package main;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Session;

import fr.afpa.cda.controlesSpecifiques.ControlPersonne;
import fr.afpa.cda.entiteDAO.MessageDAO;
import fr.afpa.cda.entiteDAO.PersonneDAO;
import fr.afpa.cda.entiteMetier.Personne;
import fr.afpa.cda.serviceDTO.ServiceMessageDTO;
import fr.afpa.cda.serviceDTO.ServiceRechercheDto;
import fr.afpa.cda.serviceDTO.ServicesPersonneDTO;
import fr.afpa.cda.servicesDAO.ServiceMessageDAO;
import fr.afpa.cda.servicesDAO.ServiceRechercheDao;
import fr.afpa.cda.servicesDAO.ServicesPersonneDAO;
import fr.afpa.cda.servicesMetier.ServiceMessageMetier;
import fr.afpa.cda.servicesMetier.ServiceRecherche;
import fr.afpa.cda.servicesMetier.ServicesPersonneMetier;
import fr.afpa.cda.utils.HibernateUtils;

public class Main {

	public static void main(String[] args) {
		Session s = HibernateUtils.getSession();
		ServiceRecherche srech =new ServiceRecherche();
		ServicesPersonneMetier servPersMet=new ServicesPersonneMetier();
		
		//System.out.println(servPersMet.creationPersonne("TEST", "SOUSTEST", "02/02/1990", false, "Login", "mdp"));
		ServicesPersonneDTO dtoServ =new ServicesPersonneDTO();
		ServiceRechercheDto dtoSearch = new ServiceRechercheDto();
		
		ServiceRechercheDao servDaoRech =new ServiceRechercheDao();
		ServiceMessageDAO serMessDAO =new  ServiceMessageDAO();
		ServicesPersonneDAO servPersDAO= new ServicesPersonneDAO();
		
		 
		ServiceMessageMetier servMessMetier =new ServiceMessageMetier();
		ServiceMessageDTO servMessDTO =new ServiceMessageDTO();
		
		//System.out.println(serMessDAO.recupListeMessagesEnvoyesDAO("recus",servDaoRech.recupUneSeulePersonne("Zmarine")));
		//System.out.println(srech.rechercheByLoginOrStatus("login", "Zmarine"));
		//System.out.println(srech.rechercheUnePersonne("Zmarine"));
		List<String>listeLog=new ArrayList<String>();
		//listeLog.add("Fay");
		//listeLog.add("Zalex");
		
		
		//System.out.println(servMessMetier.creerMessage("Zmarine", "Test SERV METIER", "PINKPONK", listeLog));
		
		//System.out.println(servMessMetier.archiverMessage(8));
		//MessageDAO msgD=new MessageDAO(45, LocalDateTime.now(), "temp", "voir si date fonctionne", servDaoRech.recupUneSeulePersonne("Zmarine"), servDaoRech.recupUneSeulePersonne("Shi"), false, false);
		//serMessDAO.saveMessage(msgD);  
		
		//PersonneDAO persDAO = servDaoRech.recupUneSeulePersonne("Zmarine");
		//
		//System.out.println(persDAO+" PERSONNE RECUP");
		
	
		//Personne persMETIER = dtoSearch.recupUnePersonne("Zmarine");
		//System.out.println(servMessMetier.listerMessagesSelonType("recus","Zalex" ));
		
		
		//System.out.println(persMETIER+" PERSONNE METIER");
		//persMETIER.setNom("Zimmer");
		
		//System.out.println(dtoSearch.rechercheByLoginOrStatus("actif", "false"));
		
		//System.out.println(dtoServ.personneMetierToPersonneDao(persMETIER)+" PERSONNE RETRANSFORME en DAO");
		
		//dtoServ.updatePersonne(persMETIER);
		
		//perstest.setNom("XXXXX");
		//servPersDAO.updatePerson(perstest);
	}

}

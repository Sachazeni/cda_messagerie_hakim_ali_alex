package fr.afpa.cda.controlesSpecifiques;

import fr.afpa.cda.entiteMetier.Personne;
import fr.afpa.cda.servicesMetier.ServiceRecherche;

public class ControlPersonne {
	/**
	 * Controle utilise lors de la creation d'un user pour savoir si le login est
	 * disponible
	 * 
	 * @param login : login entrée par l'utilisateur
	 * @return true si dispo, false dans le cas contraire
	 */
	public static boolean loginDisponible(String login) {
		Personne pers = new ServiceRecherche().rechercheUnePersonne(login);
		boolean flag = false;
		if (pers == null) {
			flag = true;
		}
		return flag;

	}

	/**
	 * Controle utilisé pour verifier la connexion
	 * 
	 * @param login
	 * @param mdp
	 * @return
	 */
	public static Boolean controlAuthentification(String login, String mdp) {
		Personne pers = new ServiceRecherche().rechercheUnePersonne(login);
		boolean flag = false;
		if (!loginDisponible(login) && mdp.equals(pers.getCompte().getMotDePasse())) {
			flag=true;
		}
		return flag;

	}
}

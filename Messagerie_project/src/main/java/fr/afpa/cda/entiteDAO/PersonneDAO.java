package fr.afpa.cda.entiteDAO;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@ToString(of= {"idPersonne","nom","prenom", "dateDeNaissance","compte"})
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name="personne")
@NamedQuery(name = "rechercheByLogin",query = "from PersonneDAO where login = :login")
@NamedQuery(name="rechercheDesPersStatus", query ="from PersonneDAO where actif=:actif")
@NamedQuery(name = "getAll", query = "from PersonneDAO")
public class PersonneDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "personne_generator")
	@SequenceGenerator(name = "personne_generator", sequenceName = "personne_seq",allocationSize = 1, initialValue = 2)
	@Column (name="idpersonne", updatable = false, nullable = false )
	int idPersonne;
	
	@Column(name="nom", updatable = true, nullable = false,length = 50)
	String nom;
	
	@Column (name="prenom", updatable = true, nullable = false,length = 50)
	String prenom;
	
	@Column (name="date_naissance", updatable = true, nullable = false)
	LocalDate dateDeNaissance;
	
	@Column(name ="admin", nullable = false, updatable = true, columnDefinition = "bool default false")
	boolean admin;
	
	@Column(name ="actif", nullable = false, updatable = true, columnDefinition = "bool default false")
	boolean actif;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name="login",unique = true)
	CompteDAO compte;

	@OneToMany(mappedBy ="idDestinataire", cascade = CascadeType.ALL, orphanRemoval = true )
	@OnDelete( action = OnDeleteAction.CASCADE )
	List<MessageDAO> listeMessageRecu;
	
	@OneToMany(mappedBy ="idExpediteur", cascade = CascadeType.ALL, orphanRemoval = true )
	@OnDelete( action = OnDeleteAction.CASCADE )
	List<MessageDAO> listeMessageEnvoye;

	

	
}

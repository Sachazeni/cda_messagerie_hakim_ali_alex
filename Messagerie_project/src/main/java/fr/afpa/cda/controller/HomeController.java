package fr.afpa.cda.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.cda.controlesSpecifiques.ControlPersonne;
import fr.afpa.cda.entiteMetier.Personne;
import fr.afpa.cda.servicesMetier.ServiceMessageMetier;
import fr.afpa.cda.servicesMetier.ServiceRecherche;
import fr.afpa.cda.servicesMetier.ServicesPersonneMetier;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	private static Personne personneAuthentifie;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/")
	public String connexion() {

		return "connexion";
	}

	@RequestMapping(value = "/accueil", method = RequestMethod.POST, params = { "login", "password" })
	public String connexion(@RequestParam(value = "login") String login,
			@RequestParam(value = "password") String password, Model model) {

		if (ControlPersonne.controlAuthentification(login, password)) {

			personneAuthentifie = new ServiceRecherche().rechercheUnePersonne(login);
			// si admin actif
			if (personneAuthentifie.getAdmin() && personneAuthentifie.getActif()) {
				return "accueilAdministrateur";

			} else if (!personneAuthentifie.getAdmin() && !personneAuthentifie.getActif()) {
				return "connexion";
				// personne active
			} else if (personneAuthentifie.getActif()) {
				model.addAttribute("listeMessageRecu", new ServiceMessageMetier().listerMessagesSelonType("recus",
						personneAuthentifie.getCompte().getLogin()));
				return "accueilMessagerie";
			}

		}
		return "connexion";

	}

	@RequestMapping(value = "/accueilMessagerie")
	public String accederMessagerie(Model model) {

		model.addAttribute("listeMessageRecu", new ServiceMessageMetier().listerMessagesSelonType("recus",
				personneAuthentifie.getCompte().getLogin()));

		return "accueilMessagerie";
	}

	@RequestMapping(value = "/messageEnvoye", method = RequestMethod.GET)
	public String accederMessageEnvoye(Model model) {

		model.addAttribute("listeMessageEnvoye", new ServiceMessageMetier().listerMessagesSelonType("envoyes",
				personneAuthentifie.getCompte().getLogin()));

		return "accueilMessagerie";
	}

	@RequestMapping(value = "/nouveauMessage", method = RequestMethod.GET)
	public String ecrireNouveauMessage(Model model) {

		return "nouveauMessage";
	}

	@RequestMapping(value = "/messageRecu", method = RequestMethod.GET)
	public String accederMessageRecu(Model model) {

		model.addAttribute("listeMessageRecu", new ServiceMessageMetier().listerMessagesSelonType("recus",
				personneAuthentifie.getCompte().getLogin()));

		return "accueilMessagerie";
	}

	@RequestMapping(value = "/creationCompte")
	public String nouveauClient(Model model) {

		return "creationCompte";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, params = { "login", "password", "nom", "prenom",
			"dateNaissance" })
	public String ajouterUtilisateur(@RequestParam(value = "login") String login,
			@RequestParam(value = "password") String password, @RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom,
			@RequestParam(value = "dateNaissance") String dateNaissance) {

		ServicesPersonneMetier servicesPersonneMetier = new ServicesPersonneMetier();

		servicesPersonneMetier.creationPersonne(nom, prenom, dateNaissance, false, login, password);

		return "validationCreationCompte";
	}

	
	/**
	 * ENVOYER message
	 * @param listeDestinataires
	 * @param sujet
	 * @param contenuMsg
	 * @return
	 */
	@RequestMapping(value = "/nouveauMesg", method = RequestMethod.POST)
	public String creerNouveauMessage(@RequestParam(value = "destinataireLog") String listeDestinataires,
			@RequestParam(value = "sujet") String sujet, @RequestParam(value = "contenuMessage") String contenuMsg) {
			
		String login = personneAuthentifie.getCompte().getLogin();
		
		ServiceMessageMetier servMsgMetier = new ServiceMessageMetier();
		servMsgMetier.creerMessage(login, sujet, contenuMsg, listeDestinataires);
		return "accueilMessagerie";
	}

}

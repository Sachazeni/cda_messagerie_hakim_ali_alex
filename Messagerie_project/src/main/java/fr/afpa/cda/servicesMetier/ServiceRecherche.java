package fr.afpa.cda.servicesMetier;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.cda.entiteMetier.Personne;
import fr.afpa.cda.serviceDTO.ServiceRechercheDto;


public class ServiceRecherche {


	/**
	 * Service de recherche de personne via le login ou son status
	 * @param typeRecherche: soit login soit actif, type de recherche pour recuperer une liste de personnes specifiques via le type et la valeur.
	 * @param valeur :  le login ou "true/false"
	 * @return une liste d'entité métier personne,  correspondant à la recherche
	 */
	public List<Personne> rechercheByLoginOrStatus(String typeRecherche,String valeur) {
		List<Personne> liste = new ArrayList<Personne>();
		liste = new ServiceRechercheDto().rechercheByLoginOrStatus(typeRecherche, valeur);
		return liste;
	}

	/**
	 * Service qui renvoie la liste de tous les personnnes
	 * @return une liste d'entité métier personne,  correspondant à tous les personnes de la base de donnée
	 */
	public List<Personne> getAllPersonnes() {
		List<Personne> liste = new ArrayList<Personne>();
		liste = new ServiceRechercheDto().getAllPersonnes();
		return liste;
	}
	
	/**
	 * Service qui recupere une personne via son login
	 * @param login : login de la personne:
	 * @return
	 */
	public Personne rechercheUnePersonne(String login) {
	Personne personne=new ServiceRechercheDto().recupUnePersonne(login);
	return personne;
	}
	
	
	
	
}

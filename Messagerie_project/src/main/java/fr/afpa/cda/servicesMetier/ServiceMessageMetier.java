package fr.afpa.cda.servicesMetier;

import java.time.LocalDateTime;
import java.util.List;

import fr.afpa.cda.entiteMetier.Message;
import fr.afpa.cda.entiteMetier.Personne;
import fr.afpa.cda.serviceDTO.ServiceMessageDTO;

public class ServiceMessageMetier {

	/**
	 * Methode pour la creation de d'un message
	 * 
	 * @param loginExpediteur
	 * @param sujet
	 * @param contenu
	 * @param personnesDestinataires
	 */
	public Message creerMessage(String loginExpediteur, String sujet, String contenu,
			String loginDest) {
		String affichageUtilisateur = "";

		// verification que la liste n'est pas vide
		//if (personnesDestinataires.size() != 0) {

			// si elle est pas vide debut init:
			Personne destinataire = null;
			Personne expediteur = null;
			ServiceRecherche sRechereche = new ServiceRecherche();
			ServiceMessageDTO sDtoEnvoi = new ServiceMessageDTO();

			// init l'expediteur du message
			expediteur = sRechereche.rechercheUnePersonne(loginExpediteur);
			
			Message msg = new Message();
			msg.setSujet(sujet);
			msg.setContenu(contenu);
			msg.setDate(LocalDateTime.now());
			msg.setArchiveDestinataire(false);
			msg.setArchiveExpediteur(false);
			msg.setIdMessage(0);
			msg.setPersonneExpediteur(expediteur);
			
			// prevoir un moyen d'integrer les messages dans la liste via jstl dans la jsp
			//for (String loginDest : personnesDestinataires) {

				destinataire = sRechereche.rechercheUnePersonne(loginDest);
				msg.setPersonneDestinataire(destinataire);
				System.out.println(msg+ "SERVICE MESS "+ destinataire+"***************************************************************************************************************");
				
				sDtoEnvoi.envoyerMessage(msg);
			
				
//				if (destinataire != null) {
//					if (sDtoEnvoi.envoyerMessage(msg)) {
//						affichageUtilisateur = "Message envoyé à tous les destinataires";
//					} else {
//						affichageUtilisateur = "Une erreur avec l'adresse suivante : " + loginDest;
//					}
//				} else {
//					affichageUtilisateur = "Ce login n'existe pas " + loginDest;
//				}

			///}

//		} else {
//			affichageUtilisateur = "Le champ destinataire est vide!";
//		}
		return msg;
	}

	/**
	 * Methode pour archiver un message
	 * 
	 * @param idMessage
	 * @return
	 */
	public boolean archiverMessage(Personne personneConnecte, int idMessage) {
		ServiceMessageDTO serviceMessageDTO = new ServiceMessageDTO();
		Message message = serviceMessageDTO.recupMessage(idMessage);

		// verif que le message est pas null avant de continuer
		if (message != null) {

			// comparaison entre le id de la personne qui effectue l'operation et l'id du
			// destinataire
			// a voir si on archive les messages reçus?
			if (personneConnecte.getIdPersonne() == message.getPersonneDestinataire().getIdPersonne()) {

				// archiv du message du cote du destinataire.

				message.setArchiveDestinataire(true);
				serviceMessageDTO.archiverMessage(message);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Methode pour retourner la liste de messages selon le type de recherche
	 * choisi.
	 * 
	 * @param typeMessage:"recus" pour listes les messages recus. "envoyes" pour
	 *                            lister les messages envoyés;
	 * @param login               :login de la personne connecté
	 * @return la liste voulue
	 */
	public List<Message> listerMessagesSelonType(String typeMessage, String login) {
		ServiceRecherche sRechereche = new ServiceRecherche();
		Personne personne = sRechereche.rechercheUnePersonne(login);

		List<Message> listeMessages = new ServiceMessageDTO().listeMessagesDaoVersListeMetierSelonType(typeMessage,
				personne);
		return listeMessages;
	}

	/**
	 * Methode pour retourner le message selectionnée
	 * 
	 * @param idMessage: le id du message recupere à la page jsp
	 * @return le message trouvé qu'il faut afficher dans les pages jsp
	 */
	public Message afficherMessage(int idMessage) {
		ServiceMessageDTO serviceMessageDTO = new ServiceMessageDTO();
		return serviceMessageDTO.recupMessage(idMessage);
	}
}
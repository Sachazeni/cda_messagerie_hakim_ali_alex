 package fr.afpa.cda.servicesMetier;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import fr.afpa.cda.controlesSpecifiques.ControlPersonne;
import fr.afpa.cda.entiteMetier.Compte;
import fr.afpa.cda.entiteMetier.Personne;
import fr.afpa.cda.serviceDTO.ServicesPersonneDTO;

public class ServicesPersonneMetier {

	/**
	 * Service pour le update de la personne
	 * 
	 * @param nom    : nom modifié ou non
	 * @param prenom : prenom ou non
	 * @param date   : date ou non
	 * @param login  : login ou non
	 */
	public boolean updatePers(String nom, String prenom, String date, Boolean actif, String motDePasse, String login) {
		//ICI NORMALEMENT ON RECUPERE LES REQUEST les parm en dur plus haut sont pour faire les test
		LocalDate date2=null;
		Personne personne = new ServiceRecherche().rechercheUnePersonne(login);
		
		//TODO controle deuxieme couche ici
		
		if(personne!=null) {
			
		date2=LocalDate.parse(date,  DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		personne.setNom(nom);
		personne.setPrenom(prenom);
		personne.setActif(actif);
		personne.getCompte().setMotDePasse(motDePasse);
		personne.setDateDeNaissance(date2);
		}
		return new ServicesPersonneDTO().updatePersonne(personne);
		
		

	}
	/**
	 * Service de creation d'une personne
	 * @param nom : le nom de la personne 
	 * @param prenom : le prenom de la personne 
	 * @param dateDeNaissance : la date de naissance de la personne
	 * @param login : le login de la personne 
	 * @param mdp : le mot de passe de la personne  
	 * @return : true si la personne est bien crée, false sinon
	 */
	public boolean creationPersonne(String nom , String prenom, String dateDeNaissance, boolean actif, String login, String mdp) {
		Personne personne = null;
		Compte compte = null;

	    LocalDate date = null;
	    
				if(ControlPersonne.loginDisponible(login)) {
				//init compte
				compte=new Compte();
				compte.setLogin(login);
				compte.setMotDePasse(mdp);
				System.out.println(dateDeNaissance);
				
				date=LocalDate.parse(dateDeNaissance,  DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				

			
				//init Personne
				personne = new Personne();
				personne.setNom(nom);
				personne.setPrenom(prenom);
				personne.setDateDeNaissance(date);
				personne.setActif(actif);
				personne.setCompte(compte);
				new ServicesPersonneDTO().savePersonne(personne);
				}
				return false;
		
			
		
	
	}
}

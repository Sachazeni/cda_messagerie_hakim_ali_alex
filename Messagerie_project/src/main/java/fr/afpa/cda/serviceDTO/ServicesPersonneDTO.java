package fr.afpa.cda.serviceDTO;

import fr.afpa.cda.entiteDAO.PersonneDAO;
import fr.afpa.cda.entiteMetier.Personne;
import fr.afpa.cda.servicesDAO.ServicesPersonneDAO;

public class ServicesPersonneDTO {



	
	/**
	 * Lien entre le service de modification d'une personne et le service dao qui va 
	 * mettre à jour la personne Dao dans la base de données
	 * @param personne : l'entite metier Personne initialisé auparavant
	 * @return : true si l'update s'est à été effectué, false sinon
	 */
	public boolean updatePersonne(Personne personne) {
		return new ServicesPersonneDAO().updatePerson(personneMetierToPersonneDao(personne));
	}
	
	
	
	/**
	 * méthode qui permet de transformer une entité personne metier en entité
	 * personneDao
	 * 
	 * @param personne : entité personne metier à transformer
	 * @return : une entité personneDao correspondante à l'entite personne metier
	 */
	public static PersonneDAO personneMetierToPersonneDao(Personne personne) {
		PersonneDAO personneDao = new PersonneDAO();
		
		//init de la Personne Dao ici (sans autre entites)
		personneDao.setIdPersonne(personne.getIdPersonne());
		personneDao.setNom(personne.getNom());
		personneDao.setPrenom(personne.getPrenom());
		personneDao.setActif(personne.getActif());
		personneDao.setDateDeNaissance(personne.getDateDeNaissance());
		
		//init de la Personne avec le compte (qu'on va recuperer transformé dans le service specifique)
		personneDao.setCompte(ServiceCompteDTO.compteMetierVersCompteDAO(personne.getCompte()));
		
		return personneDao;
	}

	/**
	 * Méthode qu transformé une entité personneDAO en une entité personne
	 * metier
	 * 
	 * @param personneDao : entité personneDAO à transformer
	 * @return : une entité personne métier correspondante à l'entité personneDao
	 */
	public static Personne personneDaoToPersonneMetier(PersonneDAO personneDao) {
		Personne personne = new Personne();
		personne.setIdPersonne(personneDao.getIdPersonne());
		personne.setNom(personneDao.getNom());
		personne.setPrenom(personneDao.getPrenom());
		personne.setActif(personneDao.isActif());
		personne.setAdmin(personneDao.isAdmin());
		
		personne.setCompte(ServiceCompteDTO.authDaoToAuthMetier(personneDao.getCompte()));
		personne.setDateDeNaissance(personneDao.getDateDeNaissance());
		

		return personne;
	}

	/**
	 * Lien entre le service de création d'une personne et le service dao qui va 
	 * créé la personne Dao dans la base de données
	 * @param personne
	 * @return
	 */
	public boolean savePersonne(Personne personne) {
		PersonneDAO personneDao = personneMetierToPersonneDao(personne);
		return new ServicesPersonneDAO().savePersonne(personneDao);
		
	}
	
	
	
}

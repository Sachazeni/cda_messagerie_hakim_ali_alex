package fr.afpa.cda.serviceDTO;

import java.util.List;
import java.util.stream.Collectors;

import fr.afpa.cda.entiteDAO.PersonneDAO;
import fr.afpa.cda.entiteMetier.Personne;
import fr.afpa.cda.servicesDAO.ServiceRechercheDao;

public class ServiceRechercheDto {

	/**
	 * Service DTO qui execute la requete de recherche personneDAO via le login ou
	 * le status (actif ou inactif de la personne) transforme la liste d'entité
	 * PersonneDao renvoyé par le DAO en liste d'entité personne metier avant de la
	 * renvoyer au service metier
	 * 
	 * @param typeRecherche
	 * @param valeur        : login si typeRecherche=login, true ou false si type de
	 *                      recherche=actif
	 * 
	 * @return : une liste d'entités personnes metiers, correspondant à la recherche
	 */
	public List<Personne> rechercheByLoginOrStatus(String typeRecherche, String valeur) {
		List<PersonneDAO> listePersDao = new ServiceRechercheDao().rechercheByLoginOrActif(typeRecherche, valeur);
		List<Personne> listePers = null;
		if (listePersDao != null) {
			listePers = listePersDao.stream().map(ServicesPersonneDTO::personneDaoToPersonneMetier)
					.collect(Collectors.toList());
		}
		return listePers;
	}
	

	/**
	 * service DTO qui sert de lien entre le service metier de recuperation de
	 * toutes les personnes et le service dao, transforme la liste d'entité
	 * PersonneDao renvoyé par le DAO en liste d'entité personne metier avant de la
	 * renvoyer au service metier
	 * 
	 * @return une liste d'entités personnes metiers
	 */
	public List<Personne> getAllPersonnes() {
		List<PersonneDAO> listePersDao = new ServiceRechercheDao().getAllPersonnes();
		List<Personne> listePers = null;
		if (listePersDao != null) {
			listePers = listePersDao.stream().map(ServicesPersonneDTO::personneDaoToPersonneMetier)
					.collect(Collectors.toList());
		}
		return listePers;
	}
	
	/**
	 * Methode pour recuperer une seule personne via son login
	 * @param login 
	 * @return
	 */
	public Personne recupUnePersonne(String login) {
		ServiceRechercheDao servDaoRech =new ServiceRechercheDao();
		
		PersonneDAO persDAO=null;
		
		Personne personneMetier =null;
		persDAO = servDaoRech.recupUneSeulePersonne(login);
		if(persDAO!=null) {
			personneMetier=ServicesPersonneDTO.personneDaoToPersonneMetier(persDAO);
			
		}
	
		return personneMetier;
	}
}

package fr.afpa.cda.serviceDTO;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.cda.entiteDAO.MessageDAO;
import fr.afpa.cda.entiteDAO.PersonneDAO;
import fr.afpa.cda.entiteMetier.Message;
import fr.afpa.cda.entiteMetier.Personne;
import fr.afpa.cda.servicesDAO.ServiceMessageDAO;

public class ServiceMessageDTO {

	/**
	 * Methode pour transformer un message metier vers une entite persistante dans
	 * la base de données
	 * 
	 * @param messageMetier : le message à transformer (pour l'envoye du message)
	 * @return msgDao: le message transformé en une entité persistante
	 */
	public static MessageDAO messageMetierVersMessDao(Message messageMetier) {
		MessageDAO msgDao = new MessageDAO();

		// set tous les attributs
		msgDao.setIdMessage(messageMetier.getIdMessage());
		msgDao.setSujet(messageMetier.getSujet());
		msgDao.setContenu(messageMetier.getContenu());
		msgDao.setArchiveExpediteur(messageMetier.isArchiveExpediteur());
		msgDao.setArchiveDestinataire(messageMetier.isArchiveDestinataire());
		msgDao.setDateEnvoi(messageMetier.getDate());
		msgDao.setIdExpediteur(ServicesPersonneDTO.personneMetierToPersonneDao(messageMetier.getPersonneExpediteur()));
		msgDao.setIdDestinataire(
				ServicesPersonneDTO.personneMetierToPersonneDao(messageMetier.getPersonneDestinataire()));

		return msgDao;
	}

	/**
	 * Methode pour l'archivage du message pris en parametre
	 * 
	 * @param messageMetier
	 * @return
	 */
	public static MessageDAO archiveMetierVersarchiveDao(Message messageMetier) {
		MessageDAO msgDao = new MessageDAO();

		msgDao.setArchiveDestinataire(messageMetier.isArchiveDestinataire());

		return msgDao;
	}

	/**
	 * Methode pour retourner la liste de messages selon le type de recherche
	 * choisi.
	 * 
	 * @param typeMessage :"recus" pour listes les messages recus. "envoyes" pour
	 *                    lister les messages envoyés;
	 * @param pers
	 * @return
	 */
	public List<Message> listeMessagesDaoVersListeMetierSelonType(String typeMessage, Personne pers) {
		PersonneDAO persDao = ServicesPersonneDTO.personneMetierToPersonneDao(pers);
		List<Message> listeMessagesMetier = new ArrayList<Message>();
		List<MessageDAO> listeMessagesDao = new ServiceMessageDAO().recupListeMessagesEnvoyesDAO(typeMessage, persDao);

		if (listeMessagesDao.size() != 0) {

			for (MessageDAO msgDao : listeMessagesDao) {
				listeMessagesMetier.add(mappingMessageDaoToMessageMetier(msgDao));
			}
		}
		return listeMessagesMetier;
	}

	/**
	 * Création d'un message reliant la methode DTO avec le DAO
	 * 
	 * @param message
	 * @return true si l'envoi à a abouti
	 */
	public boolean envoyerMessage(Message message) {
		MessageDAO msgDao = messageMetierVersMessDao(message);
		return new ServiceMessageDAO().saveMessage(msgDao);

	}

	/**
	 * Methode pour archiver un message
	 * 
	 * @param idMessage : l'id du message à modifier
	 * @return vrai si ok faux dans le cas contraire
	 */
	public boolean archiverMessage(Message message) {
		ServiceMessageDAO serviceMessageDAO = new ServiceMessageDAO();
		MessageDAO msgDao = messageMetierVersMessDao(message);
		return serviceMessageDAO.archiverMessage(msgDao);
	}

	/**
	 * Methode pour recuperer un message via son id
	 * 
	 * @param idMessage
	 * @return le message voulu
	 */
	public Message recupMessage(int idMessage) {
		ServiceMessageDAO serviceMessageDAO = new ServiceMessageDAO();
		MessageDAO messageDAO = serviceMessageDAO.recupererUnMessage(idMessage);
		Message message = mappingMessageDaoToMessageMetier(messageDAO);
		return message;

	}

	/**
	 * Methode pour transformer un messageDao vers un message Metier
	 * 
	 * @param messageDAO
	 * @return message Metier transforme
	 */
	private Message mappingMessageDaoToMessageMetier(MessageDAO messageDAO) {
		Message message = new Message();

		message.setIdMessage(messageDAO.getIdMessage());
		message.setSujet(messageDAO.getSujet());
		message.setContenu(messageDAO.getContenu());
		message.setDate(messageDAO.getDateEnvoi());
		message.setArchiveExpediteur(messageDAO.isArchiveExpediteur());
		message.setArchiveDestinataire(messageDAO.isArchiveDestinataire());
		message.setPersonneExpediteur(ServicesPersonneDTO.personneDaoToPersonneMetier(messageDAO.getIdExpediteur()));
		message.setPersonneDestinataire(
				ServicesPersonneDTO.personneDaoToPersonneMetier(messageDAO.getIdDestinataire()));

		return message;

	}
}

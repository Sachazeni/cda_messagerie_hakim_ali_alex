package fr.afpa.cda.serviceDTO;

import fr.afpa.cda.entiteDAO.CompteDAO;
import fr.afpa.cda.entiteMetier.Compte;
import fr.afpa.cda.entiteMetier.Personne;
import fr.afpa.cda.servicesDAO.ServiceCompteDAO;

public class ServiceCompteDTO {
	/**
	 * service Compte Dto, transforme l'entité compte en parametre en entité
	 * CompteDAO, ensuite appel le service DAO authentification pour vérifier que
	 * l'authentification est correcte si le service DAO renvoie un CompteDAO, il
	 * transforme la personne correspondante au compte en entité personne et la
	 * renvoie
	 * 
	 * @param compte : compte metier à controler
	 * @return une entité personne metier si le compte entre est correct, null sinon
	 */
	public Personne authentification(Compte auth) {
		Personne personneAuth = null;
		CompteDAO authDaoVerif = compteMetierVersCompteDAO(auth);
		CompteDAO authDaoRecup = new ServiceCompteDAO().authentification(authDaoVerif);
		if (authDaoRecup != null) {
			personneAuth = ServicesPersonneDTO.personneDaoToPersonneMetier(authDaoRecup.getPersonne());
		}
		return personneAuth;
	}

	/**
	 * Methode qui permet de transformer une entité authentification métier en
	 * entité authentificationDao
	 * 
	 * @param auth : entité authentification métier à transformer
	 * @return : une entité authentificationDao correspondante à l'entité
	 *         authentification métier
	 */
	public static CompteDAO compteMetierVersCompteDAO(Compte compte) {
		CompteDAO compteDao = new CompteDAO();
		compteDao.setLogin(compte.getLogin());
		compteDao.setMotDePasse(compte.getMotDePasse());
		return compteDao;
	}

	/**
	 * Méthode qui transforme une entité CompteDao en entité
	 * Compte métier
	 * 
	 * @param compteDao : entité Compte Dao à transformer
	 * @return : une entité Compte métier
	 */
	public static Compte authDaoToAuthMetier(CompteDAO compteDao) {
		Compte compteMetier = new Compte(compteDao.getLogin(), compteDao.getMotDePasse());
		return compteMetier;
	}

}

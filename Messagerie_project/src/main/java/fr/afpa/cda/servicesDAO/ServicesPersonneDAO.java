package fr.afpa.cda.servicesDAO;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.cda.entiteDAO.PersonneDAO;
import fr.afpa.cda.utils.HibernateUtils;

public class ServicesPersonneDAO {

	
/**
 * service de modification d'un user	
 * @param personneDao : l'entité personneDao à modifier dans la base de données 
 * @return : vrai si l'opperation a bien été effectué, faux dans le cas contraire
 */
public boolean updatePerson(PersonneDAO personneDao) {
	Session s = null;
	Transaction tx=null;
	boolean retour=false;
	try {
		s = HibernateUtils.getSession();
		tx = s.beginTransaction();
		
		s.update(personneDao);
		
		tx.commit();
		retour=true;
	}catch (Exception e) {
	Logger.getLogger(ServicesPersonneDAO.class.getName()).log(Level.SEVERE, null, e);
		
	}finally {
		if(s!=null) {
			try {
				s.close();
			} catch (Exception e) {
				Logger.getLogger(ServicesPersonneDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
	}
	return retour;
}
/**
 * service de sauvegarde d'une personne 
 * @param personneDao : l'entité personneDao à créée dans la base de données
 * @return : vrai si l'opperation à bien été effectué, faux dans le cas contraire
 */
public boolean savePersonne(PersonneDAO personneDao) {
	Session s = null;
	Transaction tx =null;
	boolean retour = false;
	try {
		s = HibernateUtils.getSession();
		tx = s.beginTransaction();
		s.save(personneDao.getCompte());
		s.save(personneDao);
		tx.commit();
		retour = true;
	}catch(Exception e) {
		
		System.out.println(e);
	}finally {
		if(s!= null) {
			try {
				s.close();
			}catch (Exception e) {
				System.out.println(e);
			}
		}
	}
	return retour;
}
}

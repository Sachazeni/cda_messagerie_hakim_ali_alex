package fr.afpa.cda.servicesDAO;

import java.util.List;



import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import fr.afpa.cda.entiteDAO.MessageDAO;
import fr.afpa.cda.entiteDAO.PersonneDAO;
import fr.afpa.cda.utils.HibernateUtils;


public class ServiceMessageDAO {
	/**
	 * Service de sauvegarde d'un message 
	 * @param MessageDao : l'entité MessageDAO à créée dans la base de données
	 * @return : vrai si l'opperation à bien été effectué, faux dans le cas contraire
	 */
	public boolean saveMessage(MessageDAO messageDao) {
		Session s = null;
		Transaction tx =null;
		boolean retour = false;
		try {
			s = HibernateUtils.getSession();
			tx = s.beginTransaction();
			s.save(messageDao);
			tx.commit();
			retour = true;
		}catch(Exception e) {
			
			System.out.println(e);
		}finally {
			if(s!= null) {
				try {
					s.close();
				}catch (Exception e) {
					System.out.println(e);
				}
			}
		}
		return retour;
	}
	
	/**
	 * Methode pour recuperer la liste des message recus ou envoyes selon le type
	 * precisée dans les parametres
	 * 
	 * @param typeMessage : "recus" pour listes les messages recus. "envoyes" pour
	 *                    lister les messages envoyés;
	 * @param pers        : la personne enregistré
	 * @return : une liste de messages
	 */
	public List<MessageDAO> recupListeMessagesEnvoyesDAO(String typeMessage, PersonneDAO pers){
		List<MessageDAO>listMessageEnvoyesDAO=null;
		Session s = null;
		
		
		try {
			s = HibernateUtils.getSession();
			Query query =null;
			
			//recup de la liste en fonction du type de recherche choisi: (attention à la casse)
			//si recherche message envoyés
			if ("envoyes".equals(typeMessage)) {
				query = s.getNamedQuery("rechercheEnvoyes");
				query.setParameter("idExpediteur", pers);
			}
			//si recherche messages reçus
			else if("recus".equals(typeMessage)) {
				query = s.getNamedQuery("rechercheRecus");
				query.setParameter("idDestinataire", pers);
			}

			if(query!=null){
			listMessageEnvoyesDAO = query.list();
			}
			
		}catch(Exception e) {
			
			System.out.println(e);
		}finally {
			if(s!= null) {
				try {
					s.close();
				}catch (Exception e) {
					System.out.println(e);
				}
			}
		}
		return listMessageEnvoyesDAO;
	}
	
	/**
	 * Methode pour l'archiver les messages via le login
	 * @param idMessage
	 * @return
	 */
	public boolean archiverMessage(MessageDAO msgDao) {
		boolean retour = false;
		Session session = null;
		Transaction transaction = null;
		session = HibernateUtils.getSession();
		
		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();
			
				session.update(msgDao);
		
			transaction.commit();			
			retour = true;

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			session.close();
		}
		return retour;

	}
	
	/**
	 * Methode pour retourner le message choisi
	 * @param idMessage
	 * @return
	 */
	public MessageDAO recupererUnMessage(int idMessage) {
		Session session = null;
		Transaction transaction = null;
		MessageDAO message = null;

		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();
			message = session.get(MessageDAO.class, idMessage);
			transaction.commit();

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			session.close();
		}
		return message;
	}
	
	
	
}


